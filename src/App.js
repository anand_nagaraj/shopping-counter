import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
import Counters from "./components/counters";
import NavBar from "./components/navbar";

class App extends Component {
  state = {
    items: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
      { id: 5, value: 0 }
    ]
  };

  constructor() {
    super();
    console.log("App - Constructor");
  }

  handleIncrement = item => {
    const items = [...this.state.items];
    const index = this.state.items.indexOf(item);
    items[index] = { ...item };
    items[index].value++;
    this.setState({ items });
  };

  handleDecrement = item => {
    const items = [...this.state.items];
    const index = this.state.items.indexOf(item);
    items[index] = { ...item };

    if (items[index].value > 0) items[index].value--;
    this.setState({ items });
  };

  handleClear = item => {
    const items = [...this.state.items];
    const index = this.state.items.indexOf(item);
    items[index] = { ...item };

    items[index].value = 0;
    this.setState({ items });
  };

  handleDelete = id => {
    const items = this.state.items.filter(item => item.id !== id);
    this.setState({ items });
  };

  handleReset = () => {
    const items = this.state.items.map(item => {
      item.value = 0;
      return item;
    });
    this.setState(items);
  };

  computeQuantity = () => {
    let sum = 0;
    this.state.items.map(item => {
      sum += item.value;
    });
    return sum;
  };

  render() {
    return (
      <div>
        <NavBar
          selectedItems={this.state.items.filter(item => item.value > 0).length}
          quantityItems={this.computeQuantity()}
        />
        <Counters
          items={this.state.items}
          onIncrement={this.handleIncrement}
          onDecrement={this.handleDecrement}
          onClear={this.handleClear}
          onDelete={this.handleDelete}
          onReset={this.handleReset}
        />
      </div>
    );
  }
}

export default App;
