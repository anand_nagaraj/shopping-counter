import React, { Component } from "react";

class Counter extends Component {
  constructor() {
    super();
    console.log("Counter - Constructor");
  }
  getBadgeClasses = value => {
    let classes = "badge m-2 badge-";
    classes += value === 0 ? "warning" : "primary";
    return classes;
  };
  formatCount() {
    const { value } = this.props.item;
    return value === 0 ? "Zero" : value;
  }
  render() {
    const { onIncrement, onDecrement, onClear, onDelete, item } = this.props;
    return (
      <div>
        <span className="badge m-2 badge-secondary"> {"Item-" + item.id} </span>
        <span className={this.getBadgeClasses(item.value)}>
          {this.formatCount()}
        </span>
        <button
          onClick={() => onIncrement(item)}
          className="btn btn-secondary btn-sm m-2"
        >
          +
        </button>
        <button
          onClick={() => onDecrement(item)}
          className="btn btn-third btn-sm m-2"
        >
          -
        </button>
        <button
          onClick={() => onClear(item)}
          className="btn btn-third btn-sm m-2"
        >
          Clear
        </button>
        <button
          onClick={() => onDelete(item.id)}
          className="btn btn-danger btn-sm m-2"
        >
          Delete
        </button>
      </div>
    );
  }
}

export default Counter;
