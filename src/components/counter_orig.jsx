import React, { Component } from "react";

class Counter extends Component {
  getBadgeClasses = value => {
    let classes = "badge m-2 badge-";
    classes += value === 0 ? "warning" : "primary";
    return classes;
  };
  formatCount() {
    const { value } = this.props.item;
    return value === 0 ? "0" : value;
  }
  render() {
    const { onIncrement, onDecrement, onClear, onDelete, item } = this.props;
    return (
      <div>
        <span className={this.getBadgeClasses(item.value)}> {item.id} </span>
        <span className={this.getBadgeClasses(item.value)}>
          {this.formatCount()}
        </span>
        <button
          onClick={() => onIncrement(item)}
          className="btn btn-secondary btn-sm m-2"
        >
          Increment
        </button>
        <button
          onClick={() => onDecrement(item)}
          className="btn btn-third btn-sm sm-2"
        >
          Decrement
        </button>
        <button
          onClick={() => onClear(item)}
          className="btn btn-third btn-sm sm-2"
        >
          Clear
        </button>
        <button
          onClick={() => onDelete(item.id)}
          className="btn btn-danger btn-sm sm-2"
        >
          Delete
        </button>
      </div>
    );
  }
}

export default Counter;
