import React, { Component } from "react";

class NavBar extends Component {
  constructor() {
    super();
    console.log("NavBar - Constructor");
  }
  render() {
    const { selectedItems, quantityItems } = this.props;
    return (
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="#">
          Navbar
          <span>
            {" "}
            ( Count {selectedItems}, Quantity {quantityItems}){" "}
          </span>
        </a>
      </nav>
    );
  }
}

export default NavBar;
