import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  constructor() {
    super();
    console.log("CountersBase - Constructor");
  }
  render() {
    const {
      items,
      onIncrement,
      onDecrement,
      onClear,
      onDelete,
      onReset
    } = this.props;
    return (
      <div>
        <button
          onClick={() => onReset()}
          className="btn btn-primary btn-sm sm-2"
        >
          RESET
        </button>
        {items.map(item => (
          <Counter
            item={item}
            key={item.id}
            onIncrement={onIncrement}
            onDecrement={onDecrement}
            onClear={onClear}
            onDelete={onDelete}
            onReset={onReset}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
